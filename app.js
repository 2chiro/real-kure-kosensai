// サーバー起動
const express = require('express')
const app = express()

/*
const fs = require('fs')
const https = require('https')

const options = {
  key: fs.readFileSync('./private/real-kure.key'),
  cert: fs.readFileSync('./private/real-kure.crt')
}

https.createServer(options, app).listen(443)
*/

app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile)
app.get('/camera', function (req, res) {
  res.render(__dirname + '/public/ar.html');
})

app.get('/api/time', (req, res) => {
  var date = new Date()
  time = String(date.getTime())
  res.json({time: time})
})


app.listen(3000, () => {
  console.log('the server is starting...', `https://real-kure.net/`)
})

// 静的ファイルを自動的に返すようルーティングする
app.use('/public', express.static(__dirname + '/public'))
app.use('/petterns', express.static(__dirname + '/public/petterns'))
app.use('/model', express.static(__dirname + '/public/model'))
app.use('/', express.static(__dirname + '/public'))

