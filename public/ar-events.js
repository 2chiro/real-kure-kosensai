AFRAME.registerComponent('markerhandler', {
  init: function () {
    const HonbuMarker = document.querySelector("#honbu-marker")
    const SeishinkanMarker = document.querySelector("#seishinkan-marker")
    const MMarker = document.querySelector("#m-marker")
    const EMarker = document.querySelector("#e-marker")
    const CMarker = document.querySelector("#c-marker")
    const AMarker = document.querySelector("#a-marker")
    const MogitenMarker = document.querySelector("#mogiten-marker")

    const HonbuModel = document.querySelector("#honbu-model")
    const HonbuText = document.querySelector("#honbu-text")

    const SeishinkanModel = document.querySelector("#seishinkan-model")
    const SeishinkanText = document.querySelector("#seishinkan-text")

    const MModel = document.querySelector("#m-model")
    const MText = document.querySelector("#m-text")

    const EModel = document.querySelector("#e-model")
    const EText = document.querySelector("#e-text")

    const CModel = document.querySelector("#c-model")
    const CText = document.querySelector("#c-text")

    const AModel = document.querySelector("#a-model")
    const AText = document.querySelector("#a-text")

    const MogitenModel = document.querySelector("#mogiten-model")
    const MogitenText = document.querySelector("#mogiten-text")

    const date = new Date('2019/11/10 23:59:59')

    HonbuMarker.addEventListener('click', function (ev, target) {
      const intersectedElement = ev && ev.detail && ev.detail.intersectedEl;
            if (HonbuModel && intersectedElement === HonbuModel) {
              document.cookie = 'real-kure-kosensai-stamp1=1; expires=' + date.toUTCString() + '; path=/;'
              /*
              const scale = HonbuModel.getAttribute('scale');
              Object.keys(scale).forEach((key) => scale[key] = scale[key] + 0.005);
              HonbuModel.setAttribute('scale', scale);
              */
              HonbuText.setAttribute('value', 'Thank you.')
              HonbuText.setAttribute('color', '#FF0000')
            }
    })

    SeishinkanMarker.addEventListener('click', function (ev, target) {
      const intersectedElement = ev && ev.detail && ev.detail.intersectedEl;
            if (SeishinkanModel && intersectedElement === SeishinkanModel) {
              document.cookie = 'real-kure-kosensai-stamp2=1; expires=' + date.toUTCString() + ' path=/;'
              /*
              const scale = SeishinkanModel.getAttribute('scale');
              Object.keys(scale).forEach((key) => scale[key] = scale[key] + 0.01);
              SeishinkanModel.setAttribute('scale', scale);
              */
              SeishinkanText.setAttribute('value', 'Thank you.')
              SeishinkanText.setAttribute('color', '#FF0000')
            }
    })

    MMarker.addEventListener('click', function (ev, target) {
      const intersectedElement = ev && ev.detail && ev.detail.intersectedEl;
            if (MModel && intersectedElement === MModel) {
              document.cookie = 'real-kure-kosensai-stamp3=1; expires=' + date.toUTCString() + '; path=/;'
              /*
              const scale = MModel.getAttribute('scale');
              Object.keys(scale).forEach((key) => scale[key] = scale[key] + 0.01);
              MModel.setAttribute('scale', scale);
              */
              MText.setAttribute('value', 'Thank you.')
              MText.setAttribute('color', '#FF0000')
            }
    })

    EMarker.addEventListener('click', function (ev, target) {
      const intersectedElement = ev && ev.detail && ev.detail.intersectedEl;
            if (EModel && intersectedElement === EModel) {
              document.cookie = 'real-kure-kosensai-stamp4=1; expires=' + date.toUTCString() + '; path=/;'
              /*
              const scale = EModel.getAttribute('scale');
              Object.keys(scale).forEach((key) => scale[key] = scale[key] + 0.01);
              EModel.setAttribute('scale', scale);
              */
              EText.setAttribute('value', 'Thank you.')
              EText.setAttribute('color', '#FF0000')

            }
    })

    CMarker.addEventListener('click', function (ev, target) {
      const intersectedElement = ev && ev.detail && ev.detail.intersectedEl;
            if (CModel && intersectedElement === CModel) {
              document.cookie = 'real-kure-kosensai-stamp5=1; expires=' + date.toUTCString() + '; path=/;'
              /*
              const scale = CModel.getAttribute('scale');
              Object.keys(scale).forEach((key) => scale[key] = scale[key] + 0.01);
              CModel.setAttribute('scale', scale);
              */
              CText.setAttribute('value', 'Thank you.')
              CText.setAttribute('color', '#FF0000')
            }
    })

    AMarker.addEventListener('click', function (ev, target) {
      const intersectedElement = ev && ev.detail && ev.detail.intersectedEl;
            if (AModel && intersectedElement === AModel) {
              document.cookie = 'real-kure-kosensai-stamp6=1; expires=' + date.toUTCString() + '; path=/;'
              /*
              const scale = AModel.getAttribute('scale');
              Object.keys(scale).forEach((key) => scale[key] = scale[key] + 0.01);
              AModel.setAttribute('scale', scale);
              */
              AText.setAttribute('value', 'Thank you.')
              AText.setAttribute('color', '#FF0000')
            }
    })

    MogitenMarker.addEventListener('click', function (ev, target) {
      const intersectedElement = ev && ev.detail && ev.detail.intersectedEl;
            if (MogitenModel && intersectedElement === MogitenModel) {
              document.cookie = 'real-kure-kosensai-stamp7=1; expires=' + date.toUTCString() + '; path=/;'
              /*
              const scale = MogitenModel.getAttribute('scale');
              Object.keys(scale).forEach((key) => scale[key] = scale[key] + 0.01);
              MogitenModel.setAttribute('scale', scale);
              */
              MogitenText.setAttribute('value', 'Thank you.')
              MogitenText.setAttribute('color', '#FF0000')
            }
    })
  }
})