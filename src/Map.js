import React from 'react'
import L from 'leaflet'
import { Map, ImageOverlay, Marker} from 'react-leaflet'
import 'leaflet/dist/leaflet.css'
import MapImage from './images/map2.png'

export const unregARIcon = new L.Icon({
  iconUrl: require('./assets/marker2.png'),
  iconRetinaUrl: require('./assets/marker2.png'),
  iconSize: [50, 50],
  iconAnchor: [25, 50],
  popupAnchor: [10, -44],
})

export class LMap extends React.Component {
  constructor (props) {
    super(props)
  }

  render () {
    return (
      <div id='container'>
        <Map
          ref='map'
          center={[0, 0]}
          zoom={1}
          minZoom={1}
          maxZoom={3}
          maxBounds={[[-200, -200], [200, 200]]}
          doubleClickZoom={false}
          zoomControl={false} >
          <ImageOverlay
            bounds={[[-200, -200], [200, 200]]}
            url={MapImage}
          />
          <Marker
            position={[77, -130]}
            icon={unregARIcon}
          />
          <Marker
            position={[-45, -85]}
            icon={unregARIcon}
          />
          <Marker
            position={[28, -18]}
            icon={unregARIcon}
          />
          <Marker
            position={[63, 58]}
            icon={unregARIcon}
          />
          <Marker
            position={[30, 58]}
            icon={unregARIcon}
          />
          <Marker
            position={[-20, 58]}
            icon={unregARIcon}
          />
          <Marker
            position={[-20, 165]}
            icon={unregARIcon}
          />
        </Map>
      </div>
    )
  }
}