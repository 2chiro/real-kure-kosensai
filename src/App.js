import React from 'react';
import QRCode from 'qrcode.react'
import request from 'superagent'
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Link
} from 'react-router-dom'
import './App.css';
import MainLogo from './images/icon-logo.png'
import MapLogo from './images/icon-map.png'
import CameraLogo from './images/icon-camera.png'
import StampLogo from './images/icon-stamp.png'
import classNames from 'classnames'
import {LMap} from './Map'

import HelpLogo from './images/icon-help.png'
import HelpImg from './images/help.png'

import StampHonbu from './images/StampFrame_Honbu.png'
import StampSeishinkan from './images/StampFrame_Seishinkan.png'
import StampM from './images/StampFrame_M.png'
import StampE from './images/StampFrame_E.png'
import StampC from './images/StampFrame_C.png'
import StampA from './images/StampFrame_A.png'
import StampMogiten from './images/StampFrame_Mogiten.png'

import StampWa from './images/StampFrame_Wa.png'
import StampRa from './images/StampFrame_Ra.png'
import StampI1 from './images/StampFrame_I1.png'
import StampN from './images/StampFrame_N.png'
import StampSa from './images/StampFrame_Sa.png'
import StampI2 from './images/StampFrame_I2.png'
import StampExc from './images/StampFrame_Exc.png'

import StampGet from './images/StampFrame_Get0.png'
import StampGet1 from './images/StampFrame_Get1.png'

const App = () => (
  <Router>
    <div>
      <Switch>
        <Route exact path='/' component={Home} />
        <Route path='/camera'/>
      </Switch>
    </div>
  </Router>
)

class Home extends React.Component {
  constructor (props) {
    super(props)
    this.bindFunc = this.func.bind(this)
    this.helpFunc = this.help.bind(this)
    this.state = {
      nav: 0, navBool: [true, false, false],
      Component: LMap, help: 0
    }
  }
  componentDidMount () {
    document.getElementById("helpImg").style.display="none";
  }
  func (nav) {
    const navCopy = this.state.navBool.slice()
    for (var i = 0; i < 3; i++) {
      if (i == nav) {
        navCopy[i] = true
      } else {
        navCopy[i] = false
      }
    }
    switch (nav) {
      case 0: 
        this.setState({nav: nav, navBool: navCopy, Component: LMap, help: 0});
        document.getElementById("helpImg").style.display="none";
        break
      case 2:
        this.setState({nav: nav, navBool: navCopy, Component: Stamp, help: 0});
        document.getElementById("helpImg").style.display="none";
        break
      default:
        this.setState({nav: nav, navBool: navCopy, Component: LMap, help: 0});
        document.getElementById("helpImg").style.display="none";
        break
    }
  }
  help (n) {
    if (n === 0) {
      document.getElementById("helpImg").style.display="none";
    } else {
      document.getElementById("helpImg").style.display="block";
    }
    this.setState({help: n})
  }
  render () {
    const {Component} = this.state

    var ol = document.cookie.indexOf('real-kure-kosensai-openlog')
    if (ol === -1) {
      request.get('/api/time')
      .end((error, res) => {
        if (!error) {
          console.log(res.body.time)
          var res_time = res.body.time
          const date = new Date('2019/11/10 23:59:59')
          var c = 'real-kure-kosensai-openlog=real-kure:' + res_time + '; expires=' + date.toUTCString() + ';'
          document.cookie = c
        } else {
          console.log(error)
        }
      })
    }
    return (
      <div className="App">
        <Header help={this.helpFunc} id={this.state.help}/>
        <Component />
        <Footer func={this.bindFunc} nav={this.state.navBool}/>
        <img src={HelpImg} alt="" id="helpImg"></img>
      </div>
    )
  }
}

class Header extends React.Component {
  constructor (props) {
    super(props)
    this.helpHandler = this.helpHandler.bind(this)
    this.state = {
      help: 0
    }
  }
  helpHandler () {
    var n = this.props.id === 0 ? 1 : 0
    this.props.help(n)
  }
  render () {
    return (
      <header className="App-header">
        <img src={MainLogo} alt="" className="header-logo"></img>
        <img src={HelpLogo} alt="" className="help-logo" onClick={this.helpHandler}></img>
      </header>
    )
  }
}

class Footer extends React.Component {
  constructor (props) {
    super(props)
    this.navTapHandler = this.navTapHandler.bind(this);
  }
  navTapHandler (e) {
    const k = Number(e.currentTarget.getAttribute('nav-num'))
    if (k === 1) {
      location.href = './camera' 
    } else {
      this.props.func(k)
    }
  }
  render () {
    var navMap = classNames({'current': this.props.nav[0]}, {'none': !this.props.nav[0]})
    var navCamera = classNames({'current': this.props.nav[1]}, {'none': !this.props.nav[1]})
    var navStamp = classNames({'current': this.props.nav[2]}, {'none': !this.props.nav[2]})
    return (
      <footer className="App-footer">
        <div className={navMap} onClick={this.navTapHandler} nav-num="0"><img src={MapLogo} alt="" className="navImage"></img></div>
        <div className={navCamera} onClick={this.navTapHandler} nav-num="1"><img src={CameraLogo} alt="" className="navImage"></img></div>
        <div className={navStamp} onClick={this.navTapHandler} nav-num="2"><img src={StampLogo} alt="" className="navImage"></img></div>
      </footer>
    )
  }
}

class Stamp extends React.Component {
  constructor (props) {
    super(props)
    this.canvas = this.ctx = null
    this.animationIds = null
    this.state = {
      width: null, height: null,
    }
  }
  updateDimensions () {
    this.setState({width: this.canvas.clientWidth, height: this.canvas.clientHeight})
  }
  componentDidMount () {
    if (!this.canvas) return
    this.updateDimensions()
    window.addEventListener('resize', this.updateDimensions.bind(this))
    this.initCanvas()
  }
  componentWillUnmount () {
    cancelAnimationFrame(this.animationIds)
  }
  initCanvas () {
    if (!this.ctx) this.ctx = this.canvas.getContext("2d")
    if (this.animationIds !== null) {
      cancelAnimationFrame(this.animationIds)
    }
    this.renderCanvas(this.ctx)
  }
  renderCanvas (ctx) {
    this.animationIds = requestAnimationFrame(() => {this.renderCanvas(ctx)})
    ctx.clearRect(0, 0, this.canvas.width, this.canvas.height)
    var stamp0 = document.getElementById('stamp0')
    var stamp1 = document.getElementById('stamp1')
    var stamp2 = document.getElementById('stamp2')
    var stamp3 = document.getElementById('stamp3')
    var stamp4 = document.getElementById('stamp4')
    var stamp5 = document.getElementById('stamp5')
    var stamp6 = document.getElementById('stamp6')
    var stamp7 = document.getElementById('stamp7')
    var stamp8 = document.getElementById('stamp8')
    ctx.font = "18px 'Times New Roman'"
    ctx.fillStyle = 'black'
    var message = '呉高専　第54回高専祭　スタンプラリー'
    var metrics = ctx.measureText(message);
    var textWidth = metrics.width;
    var xPosition = (this.state.width / 2) - (textWidth / 2)
    var yPosition = (this.state.height / 10)
    ctx.fillText(message, xPosition, yPosition)

    ctx.drawImage(stamp0, this.state.width / 15 , (this.state.height * 2) / 7 - 70, 110, 110)
    ctx.drawImage(stamp1, (this.state.width * 14) / 15 - 110 , (this.state.height * 2) / 7 - 70, 110, 110)
    ctx.drawImage(stamp2, this.state.width / 2 - 55, (this.state.height * 3) / 7 - 70, 110, 110)
    ctx.drawImage(stamp3, this.state.width / 15, this.state.height * 4 / 7 - 70, 110, 110)
    ctx.drawImage(stamp4, (this.state.width * 14) / 15 - 110, (this.state.height * 4) / 7 - 70, 110, 110)
    ctx.drawImage(stamp5, this.state.width / 2 - 55, (this.state.height * 5) / 7 - 70, 110, 110)
    ctx.drawImage(stamp6, this.state.width / 15, (this.state.height * 6) / 7 - 70, 110, 110)

    var s0 = document.cookie.indexOf('real-kure-kosensai-stamp1')
    if (s0 >= 0) {
      var stamp10 = document.getElementById('stamp10')
      ctx.drawImage(stamp10, this.state.width / 15 + 5, (this.state.height * 2) / 7 - 70 + 5, 110, 110)
    }
    var s1 = document.cookie.indexOf('real-kure-kosensai-stamp2')
    if (s1 >= 0) {
      var stamp11 = document.getElementById('stamp11')
      ctx.drawImage(stamp11, (this.state.width * 14) / 15 - 110 + 5 , (this.state.height * 2) / 7 - 70 + 5, 110, 110)
    }
    var s2 = document.cookie.indexOf('real-kure-kosensai-stamp3')
    if (s2 >= 0) {
      var stamp12 = document.getElementById('stamp12')
      ctx.drawImage(stamp12, this.state.width / 2 - 55 + 5, (this.state.height * 3) / 7 - 70 + 5, 110, 110)
    }
    var s3 = document.cookie.indexOf('real-kure-kosensai-stamp4')
    if (s3 >= 0) {
      var stamp13 = document.getElementById('stamp13')
      ctx.drawImage(stamp13, this.state.width / 15 + 5, this.state.height * 4 / 7 - 70 + 5, 110, 110)
    }
    var s4 = document.cookie.indexOf('real-kure-kosensai-stamp5')
    if (s4 >= 0) {
      var stamp14 = document.getElementById('stamp14')
      ctx.drawImage(stamp14, (this.state.width * 14) / 15 - 110 + 5, (this.state.height * 4) / 7 - 70 + 5, 110, 110)
    }
    var s5 = document.cookie.indexOf('real-kure-kosensai-stamp6')
    if (s5 >= 0) {
      var stamp15 = document.getElementById('stamp15')
      ctx.drawImage(stamp15, this.state.width / 2 - 55 + 5, (this.state.height * 5) / 7 - 70 + 5, 110, 110)
    }
    var s6 = document.cookie.indexOf('real-kure-kosensai-stamp7')
    if (s6 >= 0) {
      var stamp16 = document.getElementById('stamp16')
      ctx.drawImage(stamp16, this.state.width / 15 + 5, (this.state.height * 6) / 7 - 70 + 5, 110, 110)
    }

    if (s0 >= 0 && s1 >= 0 && s2 >= 0 && s3 >= 0 && s4 >= 0 && s5 >= 0 && s6 >= 0) {
      ctx.drawImage(stamp8, (this.state.width * 14) / 15 - 120, (this.state.height * 6) / 7 - 75, 150, 150)
    } else {
      ctx.drawImage(stamp7, (this.state.width * 14) / 15 - 120, (this.state.height * 6) / 7 - 75, 150, 150)
    }

  }
  clickHandler (e) {
    var x = e.clientX - this.canvas.offsetLeft
    var y = e.clientY - this.canvas.offsetTop
    if (x >= (this.state.width * 14) / 15 - 120 && x <= (this.state.width * 14) / 15 + 30 && y >= (this.state.height * 6) / 7 - 75 && y <= (this.state.height * 6) / 7 + 75) {
      var s0 = document.cookie.indexOf('real-kure-kosensai-stamp1')
      var s1 = document.cookie.indexOf('real-kure-kosensai-stamp2')
      var s2 = document.cookie.indexOf('real-kure-kosensai-stamp3')
      var s3 = document.cookie.indexOf('real-kure-kosensai-stamp4')
      var s4 = document.cookie.indexOf('real-kure-kosensai-stamp5')
      var s5 = document.cookie.indexOf('real-kure-kosensai-stamp6')
      var s6 = document.cookie.indexOf('real-kure-kosensai-stamp7')
      if (s0 >= 0 && s1 >= 0 && s2 >= 0 && s3 >= 0 && s4 >= 0 && s5 >= 0 && s6 >= 0) {
        cancelAnimationFrame(this.animationIds)
        this.setState({getQR: true})
      } else {
        this.setState({getQR: false})
      }
    } else {
      this.setState({getQR: false})
    }
  }
  render () {
    var Base
    var Image
    if (this.state.getQR && document.cookie.indexOf('real-kure-kosensai-openlog') >= 0) {
      var cookieValue = document.cookie.replace(/(?:(?:^|.*;\s*)real-kure-kosensai-openlog\s*\=\s*([^;]*).*$)|^.*$/, "$1");
      Base = <div>
        <h1 style={{color: '#F43F79'}}>Complete!!</h1>
        <p>本部テントへ行き、実行委員にこの画面を見せてください。</p>
        <QRCode value={cookieValue} />
        <p>「<b>りあるくれ</b>」をご使用いただきまして、ありがとうございました。</p>
        <p>来年のお越しをお待ちしております。</p>
      </div>
      Image = <div></div>
    } else {
      Base = 
        <canvas
        className="canvas"
        ref={(e) => { this.canvas = e }}
        width={this.state.width}
        height={this.state.height}
        onClick={(e) => this.clickHandler(e)}
      ></canvas>
      Image = <div>
        <img id='stamp0' src={StampHonbu} style={{display: 'none'}} />
        <img id='stamp1' src={StampSeishinkan} style={{display: 'none'}} />
        <img id='stamp2' src={StampM} style={{display: 'none'}} />
        <img id='stamp3' src={StampE} style={{display: 'none'}} />
        <img id='stamp4' src={StampC} style={{display: 'none'}} />
        <img id='stamp5' src={StampA} style={{display: 'none'}} />
        <img id='stamp6' src={StampMogiten} style={{display: 'none'}} />
        <img id='stamp7' src={StampGet} style={{display: 'none'}} />
        <img id='stamp8' src={StampGet1} style={{display: 'none'}} />
        <img id='stamp10' src={StampWa} style={{display: 'none'}} />
        <img id='stamp11' src={StampRa} style={{display: 'none'}} />
        <img id='stamp12' src={StampI1} style={{display: 'none'}} />
        <img id='stamp13' src={StampN} style={{display: 'none'}} />
        <img id='stamp14' src={StampSa} style={{display: 'none'}} />
        <img id='stamp15' src={StampI2} style={{display: 'none'}} />
        <img id='stamp16' src={StampExc} style={{display: 'none'}} />
      </div>
    }
    return (
      <div id='container2'>
        {Base}
        {Image}
      </div>
    )
  }
}

export default App;